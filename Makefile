install:
	docker run --rm -v `pwd`:/config -it --device=/dev/ttyUSB0 esphome/esphome ultrasonic.yaml run

monitor:
	docker run --rm -v `pwd`:/config -it --device=/dev/ttyUSB0 esphome/esphome ultrasonic.yaml logs