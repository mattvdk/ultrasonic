# Ultrasonic Sensor for Water Reservoir

Note: Sensor data seems to be inaccurate below 10cm

Adjust water threshold levels in `ultrasonic.yaml`


## Install
* Connect via USB
* run `make install`
* disconnect with `Ctrl-c`

## Monitor while running
* Connect via USB
* run `make monitor`
* disconnect with `Ctrl-c`

## Connect with Web Interface

* connect to SSID `ultrasonic`
* password `cameroon237`
* open `192.168.8.1` in web browser
